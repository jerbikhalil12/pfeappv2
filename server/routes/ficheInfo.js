const router = require('express').Router();

const checkAuth = require('../middlewares/checkAuth');
const ficheInfoController = require('../controllers/ficheInfoController');

// @Route GET /api/fiche_info
// @Desc GET ALL data from fiche_info collection
// @Access Private

router.get('/', checkAuth, ficheInfoController.getAll);

// @Route GET /api/fiche_info/:id
// @Desc GET Specific fiche info with the given ID
// @Access Private

router.get('/:id', checkAuth, ficheInfoController.getFicheInfo);

// @Route POST /api/fiche_info/add
// @Desc Add fiche info
// @Access Private

router.post('/add', checkAuth, ficheInfoController.addFicheInfo);

// @Route PUT /api/fiche_info/modify/:id
// @Desc fiche modification
// @Access Private

router.put('/modify/:id', checkAuth, ficheInfoController.modifyFicheInfo);

module.exports = router;
