const router = require('express').Router();
const authController = require('../controllers/authController');
const checkAuth = require('../middlewares/checkAuth');
const checkAdmin = require('../middlewares/checkAdmin');

// @Route POST /api/auth/login
// @Desc login users
// @Access Public
router.post('/login', authController.loginUser);

// @Route POST /api/auth/register
// @Desc Register users
// @Access Private
router.post('/register', [checkAuth, checkAdmin], authController.registerUsers);

module.exports = router;
