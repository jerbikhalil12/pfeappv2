const bcrypt = require('bcryptjs');
const _ = require('lodash');
const asyncHandler = require('../middlewares/asyncHandler');
const { User, validateSignUpInput } = require('../models/User');

module.exports.registerUsers = asyncHandler(async (req, res) => {
  const { error } = validateSignUpInput(req.body);
  if (error) return res.status(422).send(error.details[0].message);

  let user = await User.findOne({ $or: [{ email: req.body.email }, { cin: req.body.cin }] });
  if (user) return res.status(400).json({ title: 'User error', message: 'Cet utilisateur existe déjà !' });

  const newUser = new User(_.pick(req.body, ['firstname', 'lastname', 'cin', 'email', 'password', 'role']));

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(newUser.password, salt);

  newUser.password = hash;

  user = await newUser.save();
  const token = user.generateAuthToken();
  res.status(200).header('x-auth-token', token).header('access-control-expose-headers', 'x-auth-token').send({ status: 'OK', message: 'Enregistré avec succés !' });
});

module.exports.loginUser = asyncHandler(async (req, res) => {
  const user = await User.findOne({ $or: [{ email: req.body.login }, { cin: req.body.login }] });

  if (!user) return res.status(400).json({ title: 'Invalid Input', message: 'Les informations fournises sont incorrectes ! Vérifiez de nouveau !' });

  const isValidPassword = await bcrypt.compare(req.body.password, user.password);
  if (!isValidPassword) return res.status(400).json({ title: 'Invalid Input', message: 'Les informations fournises sont incorrectes ! Vérifiez de nouveau !' });

  const token = user.generateAuthToken();
  res.status(200).header('x-auth-token', token).header('access-control-expose-headers', 'x-auth-token').send(token);
});
