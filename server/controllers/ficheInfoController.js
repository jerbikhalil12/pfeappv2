const _ = require('lodash');

const { validateFicheInput, Fiche } = require('../models/Fiche_Info');
const asyncHandler = require('../middlewares/asyncHandler');

module.exports.getAll = asyncHandler(async (req, res) => {
  const allData = await Fiche.find();
  res.status(200).json(allData);
});

module.exports.getFicheInfo = asyncHandler(async (req, res) => {
  const fiche = await Fiche.findOne({ _id: req.params.id });
  if (!fiche) return res.status(404).json({ status: 'not found', message: 'La fiche demandée n\'a pas été trouvée! ' });

  res.status(200).json(fiche);
});

module.exports.addFicheInfo = asyncHandler(async (req, res) => {
  const { error } = validateFicheInput(req.body);
  if (error) return res.status(422).send(error.details[0].message);

  let newFiche = new Fiche({
    patient: _.pick(req.body.patient, ['firstname', 'lastname', 'age', 'sexe', 'numTel']),
    lieu: req.body.lieu,
    motifAppel: req.body.motifAppel,
    qualite: req.body.qualite,
    location: _.pick(req.body.location, ['lng', 'lat', 'label']),
  });

  newFiche = await newFiche.save();
  res.status(200).json({ status: 'OK', message: 'Les informations sont enregistrées avec succés!', newFiche });
});

module.exports.modifyFicheInfo = asyncHandler(async (req, res) => {
  const { error } = validateFicheInput(req.body);
  if (error) res.status(422).send(error.details[0].message);

  const updatedFiche = {
    patient: _.pick(req.body.patient, ['firstname', 'lastname', 'age', 'sexe', 'numTel']),
    lieu: req.body.lieu,
    motifAppel: req.body.motifAppel,
    qualite: req.body.qualite,
    location: _.pick(req.body.location, ['lng', 'lat', 'label']),
  };

  const fiche = await Fiche.findOneAndUpdate({ _id: req.params.id }, updatedFiche, { new: true });
  if (!fiche) res.status(404).json({ status: 'not found', message: 'La fiche demandée n\'a pas été trouvée! ' });

  res.status(200).json(fiche);
});
