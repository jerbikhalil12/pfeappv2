const mongoose = require('mongoose');
const Joi = require('joi');

const patientSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  sexe: {
    type: String,
    enum: ['HOMME', 'FEMME'],
    required: true,
  },
  numTel: {
    type: String,
    required: true,
    maxlength: 8,
  },
  adresse: {
    type: String,
  },
});

const validatePatient = (data) => {
  const schema = Joi.object().keys({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    age: Joi.number().required(),
    sexe: Joi.string().valid(['HOMME', 'FEMME']).required(),
    numTel: Joi.string().regex(/^\d{8}$/).trim().required(),
    adresse: Joi.string(),
  });

  return Joi.validate(data, schema);
};

module.exports = {
  validatePatient,
  Patient: mongoose.model('patients', patientSchema),
};
