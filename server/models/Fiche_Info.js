const mongoose = require('mongoose');
const Joi = require('joi');

const ficheInfoSchema = new mongoose.Schema({
  patient: {
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
    age: {
      type: Number,
      required: true,
    },
    sexe: {
      type: String,
      enum: ['HOMME', 'FEMME'],
      required: true,
    },
    numTel: {
      type: String,
      required: true,
      maxlength: 8,
    },
  },
  motifAppel: {
    type: String,
    required: true,
    maxlength: 255,
  },
  lieu: {
    type: String,
    required: true,
  },
  qualite: {
    type: String,
    required: true,
  },
  location: {
    label: {
      type: String,
      required: true,
    },
    lng: {
      type: String,
      required: true,
    },
    lat: {
      type: String,
      required: true,
    },
  },

});

const validateFicheInput = (data) => {
  const schema = Joi.object().keys({
    patient: {
      firstname: Joi.string().required(),
      lastname: Joi.string().required(),
      age: Joi.number().required(),
      sexe: Joi.string().valid(['HOMME', 'FEMME']).required(),
      numTel: Joi.string().regex(/^\d{8}$/).trim().required(),
    },
    location: {
      label: Joi.string().required(),
      lng: Joi.string().required(),
      lat: Joi.string().required(),
    },
    motifAppel: Joi.string().max(255).required(),
    lieu: Joi.string().required(),
    qualite: Joi.string().required(),
  });

  return Joi.validate(data, schema);
};

module.exports = {
  validateFicheInput,
  Fiche: mongoose.model('fiche_infos', ficheInfoSchema),
};
