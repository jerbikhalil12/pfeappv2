const mongoose = require('mongoose');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const config = require('../config');

const userSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: true,
  },
  cin: {
    type: String,
    min: 8,
    max: 8,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    enum: ['Médecin', 'Assistant'],
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  photoURL: {
    type: String,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
});

// eslint-disable-next-line func-names
userSchema.methods.generateAuthToken = function () {
  const payload = {
    id: this.id, isAdmin: this.isAdmin, role: this.role, fullName: `${this.firstname} ${this.lastname}`,
  };
  const token = jwt.sign(payload, config.JWT_SECRET, { expiresIn: '7d' });
  return token;
};

const validateSignUpInput = (input) => {
  const schema = Joi.object().keys({
    firstname: Joi.string().required().error(errors => console.log(errors)),
    lastname: Joi.string().required(),
    email: Joi.string().email().required(),
    role: Joi.string().valid(['Assistant', 'Médecin']).required(),
    cin: Joi.string().regex(/^\d{8}$/).required(),
    password: Joi.string().required(),
    isAdmin: Joi.boolean(),
    photoURL: Joi.string(),

  });

  return Joi.validate(input, schema);
};

module.exports = {
  User: mongoose.model('users', userSchema),
  validateSignUpInput,
};
