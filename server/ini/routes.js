const authRouter = require('../routes/auth');
const ficheInfoRouter = require('../routes/ficheInfo');
const errorHandler = require('../middlewares/error');

module.exports = (app) => {
  app.use('/api/auth', authRouter);
  app.use('/api/fiche_info', ficheInfoRouter);
  app.use(errorHandler);
};
