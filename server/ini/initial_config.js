const bodyParser = require('body-parser');
const helmet = require('helmet');
const morgan = require('morgan');
const winston = require('../ini/logging');


module.exports = (app) => {
  app.use(helmet());
  app.use(helmet.hidePoweredBy({ setTo: 'PHP/7.3.0' }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  if (process.env.NODE_ENV !== 'production') {
    app.use(morgan('combined', { stream: winston.stream }));
  }
};
