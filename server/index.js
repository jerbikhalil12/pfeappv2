const express = require('express');

const app = express();
const PORT = 5000 || process.env.PORT;

// application inital configuration
app.use('/static', express.static('public'));
require('./ini/initial_config')(app);
// database configuration
require('./ini/databse')();
// application routes
require('./ini/routes')(app);

app.listen(PORT, () => console.log(`App listening to port: ${PORT} ... `));
