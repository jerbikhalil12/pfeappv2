import * as actionTypes from './Types';
import axios from '../../Services/httpService';

export const loginUser = data => async (dispatch) => {
  try {
    const result = await axios.post('/api/auth/login', data);
    localStorage.setItem('jwt', result.headers['x-auth-token']);
    window.location = '/fiche_med';
  } catch (error) {
    dispatch({
      type: actionTypes.LOGIN_USER_FAIL,
      payload: error.response.data.message,
    });
  }
};

export const logoutUser = () => {
  localStorage.removeItem('jwt');
};
