import axios from 'axios';

axios.interceptors.response.use(null, (error) => {
  const expectedError = error.response
      && error.response.status >= 400
      && error.response.status < 500;

  if (!expectedError) {
    console.log(error);
    window.location = '/500';
  }

  return Promise.reject(error);
});

const setToken = (jwt) => {
  axios.defaults.headers.commun['x-auth-token'] = jwt;
};

export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
  setToken,
};
