import jwtDecode from 'jwt-decode';
import moment from 'moment';

const getToken = () => localStorage.getItem('jwt');

const decode = token => jwtDecode(token);

const getExipirationDate = token => moment.unix(decode(token).exp);

const isValidToken = token => moment().isBefore(getExipirationDate(token));

export const isAuthenticated = () => {
  const token = getToken();
  return !!((token && isValidToken(token)));
};
