import * as Yup from 'yup';

export const validateInput = async (name, value, Schema) => {
  const obj = { [name]: value };
  const subSchema = Yup.object().shape({ [name]: Schema.fields[name] });

  try {
    await subSchema.validate(obj);
  } catch (error) {
    return error;
  }
};

export const validateForm = (data, Schema) => !Schema.isValidSync(data);
