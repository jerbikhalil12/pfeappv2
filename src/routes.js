import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

import FicheInfo from './Pages/FicheInfo/FicheInfo';

const FicheBilan = React.lazy(() => import('./Pages/FicheBilan/FicheBilan'));
const Ambulance = React.lazy(() => import('./Pages/Ambulance/Ambulance'));
const Suivi = React.lazy(() => import('./Pages/Suivi/Suivi'));
const Stats = React.lazy(() => import('./Pages/Stats/Stats'));


const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/fiche_med/add', exact: true, name: 'Fiche médicale', component: FicheInfo },
  { path: '/fiche_bilan/add', exact: true, name: 'Fiche bilan', component: FicheBilan },
  { path: '/ambulance/track', exact: true, name: 'Ambulance', component: Ambulance },
  { path: '/suivi', exact: true, name: 'Suivi', component: Suivi },
  { path: '/stats', name: 'Stats', exact: true, component: Stats }
];

export default routes;
