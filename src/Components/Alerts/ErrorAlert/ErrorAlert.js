import React from 'react';
import { Alert } from 'reactstrap';

const ErrorAlert = ({ children, ...props }) => <Alert color="danger">{children}</Alert>;

export default ErrorAlert;