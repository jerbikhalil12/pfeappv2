import React from 'react';
import { InputGroup, InputGroupAddon, FormGroup, FormFeedback, InputGroupText, Input } from 'reactstrap';
const FormInput = ({ name, value,type, onchange,icon, placeholder, error, ...props }) => (
  <FormGroup>
    <InputGroup className="mb-3">
      <InputGroupAddon addonType="prepend">
        <InputGroupText>
          <i className={icon}></i>
        </InputGroupText>
      </InputGroupAddon>
      <Input type={type} invalid={error} name={name} placeholder={placeholder} value={value} onChange={onchange} />
      {error && <FormFeedback> {error} </FormFeedback>}
    </InputGroup>
  </FormGroup>      
);

export default FormInput;