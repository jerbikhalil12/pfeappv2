import * as actionTypes from '../../Actions/AuthActions/Types';

const initialState = {
  user: {},
  error: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_USER_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    default: return state;
  }
};

export default reducer;
