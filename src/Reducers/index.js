import { combineReducers } from 'redux';
import authReducer from './AuthReducer/AuthReducer';

export default combineReducers({
  user: authReducer,
});
