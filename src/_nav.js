export default {
  items: [
    {
      name: 'Contact',
      url: '/contact',
      icon: 'fa fa-question-circle',
      badge: {
        variant: 'success',
        text: 'Aide',
      },
    },
    {
      title: true,
      name: 'Assistant',
    },
    {
      name: 'Fiche Médicale',
      url: '/fiche_med',
      icon: 'fa fa-file-text-o',
      children: [
        {
          name: 'Créer fiche médicale',
          url: '/fiche_med/add',
          icon: 'fa fa-plus',
        },
        {
          name: 'Archives',
          url: '/fiche_med/archives',
          icon: 'fa fa-folder-open',
        },
      ],
    },
    {
      name: 'Liste Médecins',
      url: '/liste_med',
      icon: 'fa fa-user-md',
      children: [
        {
          name: 'Voir disponiblités',
          url: '/liste_med/search',
          icon: 'fa fa-search',
        },
      ],
    },
    {
      title: true,
      name: 'Helper',
    },
    {
      name: 'Gestion ambulance',
      url: '/ambulance',
      icon: 'fa fa-ambulance',
      children: [
        {
          name: 'Affecter ambulance',
          url: '/ambulance/affect',
          icon: 'fa fa-heartbeat',
        },
      ]
    },
    {
      name: 'Synchronisation',
      url: '/sync',
      icon: 'fa fa-refresh',
    },
    {
      title: true,
      name: 'Médecin',
    },
    {
      name: 'Fiche Bilan',
      url: '/fiche_bilan',
      icon: 'fa fa-file',
      children: [
        {
          name: 'Créer fiche bilan',
          url: '/fiche_bilan/add',
          icon: 'fa fa-plus',
        },
        {
          name: 'Recherche fiche bilan',
          url: '/fiche_bilan/search',
          icon: 'fa fa-search',
        },
      ]   
    },
    {
      name: 'Suivi Patient',
      url: '/suivi',
      icon: 'fa fa-file-text-o',
      children: [
        {
          name: 'Rechercher patient',
          url: '/suivi/:id',
          icon: 'fa fa-search',
        },
        {
          name: 'Statistiques',
          url: '/suivi/stats/:id',
          icon: 'fa fa-bar-chart',
        },
      ]   
    },
  ],
};
